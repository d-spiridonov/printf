NAME = libftprintf.a
FLAGS = -Wextra -Wall -Werror
INCLUDES = -I . -I ./libft
OBJECTS = 							    \
			ft_printf.o 				\
			flags_1.o 					\
			ft_itoa_base.o 				\
			flags_2.o 					\
			functions_2.o 				\
			functions_1.o 				\
			putnbrs.o 					\
			modifiers.o 				\
			types_1.o 					\
			types_2.o 					\
			functions_3.o 				\
			functions_4.o 				\
			functions_5.o               \
			functions_p.o 				\
			ft_putchar_pf.o             \
			ft_putstr_pf.o              \
			wchar_functions.o           \
										\
			./libft/ft_putnbr.o 		\
			./libft/ft_countdigits.o 	\
			./libft/ft_strchr.o 		\
			./libft/ft_isalpha.o		\
			./libft/ft_isdigit.o		\
			./libft/ft_strlen.o			\
			./libft/ft_isalnum.o 		\
			./libft/ft_strdup.o         \
			./libft/ft_strcpy.o         \
			./libft/ft_strnew.o         \
			./libft/ft_bzero.o          \
			./libft/ft_strdel.o         \
			./libft/ft_strcmp.o         \

all : $(NAME)
$(NAME) : $(OBJECTS)
	ar rc $(NAME) $(OBJECTS)
	ranlib $(NAME)
%.o : %.c
	gcc $(FLAGS) -o $@ -c $< $(INCLUDES)
clean :
	rm -f $(OBJECTS)
	rm -f *.c~
fclean : clean
	rm -f $(NAME)
re : fclean all
