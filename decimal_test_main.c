//
// Created by Dmytrii Spyrydonov on 4/14/17.
//
#include "ft_printf.h"
#include <stdio.h>
#include <locale.h>
#include <limits.h>
#define CNV "x"

int		main(void)
{
	int	my;
	int	orig;
	char c;

	setlocale(LC_ALL, "");

	my = ft_printf("%llX, %llX", 0, ULLONG_MAX);
	printf("\n");
	orig = printf("%llX, %llX", 0, ULLONG_MAX);
	printf("\n");
	printf("my1 = %d\nor1 = %d\n--------------------\n", my, orig);
//	my = ft_printf("|%#.7o|", 0);
//	printf("\n");
//	orig = printf("|%#.7o|", 0);
//	printf("\n");
//	printf("my1 = %d\nor1 = %d\n--------------------\n", my, orig);
//	my = ft_printf("{%#.5x}", 1);
//	printf("\n");
//	orig = printf("{%#.5x}", 1);
//	printf("\n");
//	printf("my1 = %d\nor1 = %d\n--------------------\n", my, orig);
//	my = ft_printf("%#.o", 42);
//	printf("\n");
//	orig = printf("%#.o", 42);
//	printf("\n");
//	printf("my1 = %d\nor1 = %d\n--------------------\n", my, orig);
//	my = ft_printf("%#.o, %#.0o", 0, 0);
//	printf("\n");
//	orig = printf("%#.o, %#.0o", 0, 0);
//	printf("\n");
//	printf("my1 = %d\nor1 = %d\n--------------------\n", my, orig);
//	my = ft_printf("|%#6.7"CNV"|\t|%#-12.7"CNV"|\t|%#.7"CNV"|\t|%#09.7"CNV"|\t|%#02.7"CNV"|", 8400,8400,0,8400,8400);
//	printf("\n");
//	orig = printf("|%#6.7"CNV"|\t|%#-12.7"CNV"|\t|%#.7"CNV"|\t|%#09.7"CNV"|\t|%#02.7"CNV"|", 8400,8400,0,8400,8400);
//	printf("\n");
//	printf("my1 = %d\nor1 = %d\n--------------------\n", my, orig);
//	my = ft_printf("|%#6.2"CNV"|\t|%#-12.2"CNV"|\t|%#.3"CNV"|\t\t|%#09.2"CNV"|\t|%#02.2"CNV"|", 8400,8400,0,8400,8400);
//	printf("\n");
//	orig = printf("|%#6.2"CNV"|\t|%#-12.2"CNV"|\t|%#.3"CNV"|\t\t|%#09.2"CNV"|\t|%#02.2"CNV"|", 8400,8400,0,8400,8400);
//	printf("\n");
//	printf("my1 = %d\nor1 = %d\n--------------------\n", my, orig);
//#define CNV "O"
//	my = ft_printf("|%#6.7"CNV"|\t|%#-12.7"CNV"|\t|%#.7"CNV"|\t|%#09.7"CNV"|\t|%#02.7"CNV"|", 8400,8400,0,8400,8400);
//	printf("\n");
//	orig = printf("|%#6.7"CNV"|\t|%#-12.7"CNV"|\t|%#.7"CNV"|\t|%#09.7"CNV"|\t|%#02.7"CNV"|", 8400,8400,0,8400,8400);
//	printf("\n");
//	printf("my1 = %d\nor1 = %d\n--------------------\n", my, orig);
//	my = ft_printf("|%#6.2"CNV"|\t|%#-12.2"CNV"|\t|%#.3"CNV"|\t\t|%#09.2"CNV"|\t|%#02.2"CNV"|", 8400,8400,0,8400,8400);
//	printf("\n");
//	orig = printf("|%#6.2"CNV"|\t|%#-12.2"CNV"|\t|%#.3"CNV"|\t\t|%#09.2"CNV"|\t|%#02.2"CNV"|", 8400,8400,0,8400,8400);
//	printf("\n");
//	printf("my1 = %d\nor1 = %d\n--------------------\n", my, orig);
//#define CNV "X"
//	my = ft_printf("|%#6.7"CNV"|\t|%#-12.7"CNV"|\t|%#.7"CNV"|\t|%#09.7"CNV"|\t|%#02.7"CNV"|", 8400,8400,0,8400,8400);
//	printf("\n");
//	orig = printf("|%#6.7"CNV"|\t|%#-12.7"CNV"|\t|%#.7"CNV"|\t|%#09.7"CNV"|\t|%#02.7"CNV"|", 8400,8400,0,8400,8400);
//	printf("\n");
//	printf("my1 = %d\nor1 = %d\n--------------------\n", my, orig);
//	my = ft_printf("|%#6.2"CNV"|\t|%#-12.2"CNV"|\t|%#.3"CNV"|\t\t|%#09.2"CNV"|\t|%#02.2"CNV"|", 8400,8400,0,8400,8400);
//	printf("\n");
//	orig = printf("|%#6.2"CNV"|\t|%#-12.2"CNV"|\t|%#.3"CNV"|\t\t|%#09.2"CNV"|\t|%#02.2"CNV"|", 8400,8400,0,8400,8400);
//	printf("\n");
//	printf("my1 = %d\nor1 = %d\n--------------------\n", my, orig);
//#define CNV "u"
//	my = ft_printf("|%#6.7"CNV"|\t|%#-12.7"CNV"|\t|%#.7"CNV"|\t|%#09.7"CNV"|\t|%#02.7"CNV"|", 8400,8400,0,8400,8400);
//	printf("\n");
//	orig = printf("|%#6.7"CNV"|\t|%#-12.7"CNV"|\t|%#.7"CNV"|\t|%#09.7"CNV"|\t|%#02.7"CNV"|", 8400,8400,0,8400,8400);
//	printf("\n");
//	printf("my1 = %d\nor1 = %d\n--------------------\n", my, orig);
//	my = ft_printf("|%#6.2"CNV"|\t|%#-12.2"CNV"|\t|%#.3"CNV"|\t\t|%#09.2"CNV"|\t|%#02.2"CNV"|", 8400,8400,0,8400,8400);
//	printf("\n");
//	orig = printf("|%#6.2"CNV"|\t|%#-12.2"CNV"|\t|%#.3"CNV"|\t\t|%#09.2"CNV"|\t|%#02.2"CNV"|", 8400,8400,0,8400,8400);
//	printf("\n");
//	printf("my1 = %d\nor1 = %d\n--------------------\n", my, orig);
}
