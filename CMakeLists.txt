cmake_minimum_required(VERSION 3.6)
project(printf)

set(CMAKE_C_FLAGS "-std=c99")
set(SOURCE_FILES ft_printf.c flags_1.c flags_2.c functions_1.c types_1.c putnbrs.c ft_itoa_base.c modifiers.c functions_2.c functions3.c types_2.c printf_main.c functions_4.c functions_p.c ft_putchar_pf.c ft_putstr_pf.c wchar_functions.c functions_5.c)

add_executable(ft_printf ${SOURCE_FILES})
target_link_libraries(ft_printf ${CMAKE_SOURCE_DIR}/libft/libft.a)