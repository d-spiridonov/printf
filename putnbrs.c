/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putnbrs.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dspyrydo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/14 20:18:15 by dspyrydo          #+#    #+#             */
/*   Updated: 2017/06/14 20:18:39 by dspyrydo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_putnbr_u(unsigned int n)
{
	unsigned int	tmp;
	char			c;

	tmp = n;
	if (tmp > 9)
	{
		ft_putnbr_u(tmp / 10);
		ft_putnbr_u(tmp % 10);
	}
	else
	{
		c = (char)(tmp + '0');
		ft_putchar(c);
	}
}
